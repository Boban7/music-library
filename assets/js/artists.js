let gridView = document.getElementById("grid-view");

let tableView = document.getElementById("table-view");
tableView.style.display = "none";

let gridViewIcon = document.getElementById("grid-icon");
gridViewIcon.style.color = "#de1a60";

let tableViewIcon = document.getElementById("table-icon");

function toggleArtistsView(view) {
    if (view == true) {

        gridView.style.display = "initial";
        gridViewIcon.style.color = "#de1a60";

        tableView.style.display = "none";
        tableViewIcon.style.color = "white";

    } 
    else {

        tableView.style.display = "table";
        tableViewIcon.style.color = "#de1a60";

        gridView.style.display = "none";
        gridViewIcon.style.color = "white";
    }
} 